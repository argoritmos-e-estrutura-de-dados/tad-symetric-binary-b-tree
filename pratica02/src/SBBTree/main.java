package SBBTree;

import java.util.Random;

/** @author messiah */
public class main {
    public static void main(String[] args) {
        /** Para cada valor de caso de teste (10.000, 20.000, 30.000, 40.000, 50.000, 60.000, 70.000,
         * 80.000, 90.000, 100.000) foi instanciada um novo objeto SBBTree.
         */
        for(int k = 0; k < 10; k++){
            System.out.println("\n\nCASO DE TESTE: "+k+"\n");
            //Gerar árvores a partir de n elementos ORDENADOS.
            System.out.println("Gerar árvores a partir de n elementos ORDENADOS:");
            for(int n = 10000; n <= 100000; n += 10000){
                SBBTree sbbt = new SBBTree();
                for(int i = 0; i < n; i++) sbbt.insert(i);
                long start_time = System.nanoTime();                //Tempo inicial.
                System.out.println("\tNúmero de comparações: "+sbbt.search(n)+" Com n:"+(n));
                System.out.println("\tTempo de Pesquisa estimado: "+(System.nanoTime()-start_time)+" ns"); //Tempo estimado para pesquisa.
            }

            //Gerar árvores a partir de n elementos ALEATÓRIOS.
            System.out.println("Gerar árvores a partir de n elemento ALEATÓRIOS:");
            for(int n = 10000; n <= 100000; n += 10000){
                SBBTree sbbt = new SBBTree();
                for(int i = 0; i < n; i++) sbbt.insert((new Random()).nextInt(n));
                //Tempo inicial para a pesquisa.
                long start_time = System.nanoTime();
                System.out.println("\tNúmero de comparações: "+sbbt.search(n+1)+" Com n:"+(n+1));
                //Tempo estimado para pesquisa.
                System.out.println("\tTempo de Pesquisa estimado: "+(System.nanoTime()-start_time)+" ns");
            }
        }
    }
}