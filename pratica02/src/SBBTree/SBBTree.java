package SBBTree;

import Item.Item;
/** @author messiah */
public class SBBTree {
    public class Node {
        /** Tipo Item genérico. */
        public Item it;
        /** Apontadores para os filhos à esquerda e à direita do nó. */
        public Node left, right;
        /** Indicam o tipo de referência (horizontal ou vertical que sai do nó. */
        public byte left_slope, right_slope;        
        /** Construtor padrão da classe Node.
         * @param it item genérico
         */
        public Node(Item it, byte ls,byte rs) {
            this.it = it;
            this.left = this.right = null;
            this.left_slope = ls;
            this.right_slope = rs;
        }
    }
    /** Nó raiz da árvore binária com balanceamento. */
    private Node root;
    /** Representam as inclinações das referências às subárvores. */
    private static final byte horizontal = 0, vertical = 1;
    /** Utilizado para verificar quando a propriedade SBB deixa se ser satisfeita. */
    private boolean property_SBB;
    /** Número de comparações na pesquisa. */
    private int number_of_comparisons;
    /** Construtor padrão da classe SBBTree. */
    public SBBTree(){ 
        this.root = null;
        this.property_SBB = true;
    }  
    
    /** Chamada publica ao método insert(Item it, Node n_father, Node n_son, boolean son_left).
     * @param i conteudo do elemento a ser inserido
     */
    public void insert(int i){ this.root = this.insert(new Item(i), null, this.root, true); }
    
    /** Chamada publica ao método search(Item it, Node n, int number_of_comparisons).
     * @param i conteudo do elemento a ser procurado
     * @return número de comparações realizadas na busca do elemento i
     */
    public int search(int i){
        this.number_of_comparisons = 0;
        if(this.search(new Item(i), this.root)) System.out.println("Item encontrado.");
        return this.number_of_comparisons;
    }
    /** Chamada publica ao método remove(Item it, Node n).
     * @param i conteúdo elemento a ser removido
     */
    public void remove(int i){ this.root = this.remove(new Item(i),this.root); }
    /** Chamada publica ao método print(Node n). */
    public void print(){ this.print(this.root); }
       
    /** PROCEDIMENTOS AUXILIARES: para garantirmos as propriedades da árvore, as
     * roações devem ser feitas conforme o necessário ápos as operações de inserção
     * e remoção. 
     */
    
    /** Deve ser efetuada quando a diferença das alturas h dos filhos de P é igual
     * a -2 e a diferença das alturas h dos filhos de FD é igual a -1 (https://upload.wikimedia.org/wikipedia/commons/b/b9/AVL_Rotação_Simples_à_Esquerda.PNG);
     * O FD deve tornar o novo pai e o nó P deve se tornar o filho da esquerda de FD.
     * @param n nó pai
     * @return 
     */
    private Node LeftLeft(Node n){
        Node nd = n.left;
        n.left = nd.right;
        nd.right = n;
        nd.left_slope = n.left_slope = this.vertical;
        n = nd;
        return n;
    }
    /** Deve ser efetuada quando a diferença das alturas h dos filhos de P é igual
     * a 2 e a diferença das alturas h dos filhos de FE é igual a -1 (https://upload.wikimedia.org/wikipedia/commons/3/3b/AVL_Rotação_Dupla_à_Direita.PNG);
     * Nesse caso devemos aplicar uma rotação à esquerda no nó FE e, em seguida,
     * uma rotação à direita no nó P.
     * @param n nó pai
     * @return 
     */
    private Node LeftRight(Node n){
        Node nd_1 = n.left;
        Node nd_2 = nd_1.right;
        nd_1.right_slope = this.vertical;
        n.left_slope = this.vertical;
        nd_1.right = nd_2.left;
        nd_2.left = nd_1;
        n.left = nd_2.right;
        nd_2.right = n;
        n = nd_2;
        return n;
    }
    /** Deve ser efetuada quando a diferença das alturas h dos filhos de P é igual
     * a 2 e a diferença das alturas h dos filhos de FE é igual a 1 (https://upload.wikimedia.org/wikipedia/commons/f/fb/AVL_Rotação_Simples_à_Direita.PNG);
     * O FE deve tornar o novo pai e o nó P deve se tornar o filho da direita de FE.
     * @param n nó pai
     * @return 
     */
    private Node RightRight(Node n) {
        Node nd = n.right;
        n.right = nd.left;
        nd.left = n;
        nd.right_slope = n.right_slope = this.vertical;
        n = nd;
        return n;
    }
    /** Deve ser efetuada quando a diferença das alturas h dos filhos de P é igual
     * a -2 e a diferença das alturas h dos filhos de FD é igual a 1 (https://upload.wikimedia.org/wikipedia/commons/2/2c/AVL_Rotação_Dupla_à_Esquerda.PNG);
     * Nesse caso devemos aplicar uma rotação à direita no nó FD e, em seguida,
     * uma rotação à esquerda no nó P.
     * @param n nó pai
     * @return 
     */
    private Node RightLeft(Node n) {
        Node nd_1 = n.right;
        Node nd_2 = nd_1.left;
        nd_1.left_slope = n.right_slope = this.vertical;
        nd_1.left = nd_2.right;
        nd_2.right = nd_1;
        n.right = nd_2.left;
        nd_2.left = n;
        n = nd_2;
        return n;
    }    
    /** A inserção inicia-se pela busca de um valor contido na árvore que seja igual
     * ao do novo elemento a ser inserido, percorrendo as sub-árvores a esquerda e
     * a direita a partir do nó raiz (Node root), por meio das chamadas recursivas;
     * Se a árvore estiver vazia ou o nó não existir, cria-se um novo nó em que 
     * será inserido as informações; Depois de inserido, altura do nó pai e de 
     * todos os nós acima devem ser atualizadas, por meio do algoritmo de rotação
     * simples.
     * @param it nova chave a ser inserida
     * @param n_father árvore pai da recursão
     * @param n_son árvore filho da recursão
     * @param  son_left filho da esquerda
     * @return árvore da recursão
     */
    private Node insert(Item it, Node n_father, Node n_son, boolean son_left){
        if(n_son == null){
            n_son = new Node(it, this.vertical, this.vertical);
            if(n_father != null)
                if(son_left) n_father.left_slope = this.horizontal;
                else n_father.right_slope = this.horizontal;
            this.property_SBB = false;
        }else if(it.compare(n_son.it) < 0){         //inserir recursividade na subárvore esquerda
            n_son.left = this.insert(it, n_son, n_son.left, true);
            if(!this.property_SBB)
                if(n_son.left_slope == this.horizontal){
                    if(n_son.left.left_slope == this.horizontal){
                        n_son = this.LeftLeft(n_son);
                        if(n_father != null)
                            if(son_left) n_father.left_slope = this.horizontal;
                            else n_father.right_slope = this.horizontal;
                    }else if(n_son.left.right_slope == this.horizontal){
                        n_son = this.LeftRight(n_son);
                        if(n_father != null)
                            if(son_left) n_father.left_slope = this.horizontal;
                            else n_father.right_slope = this.horizontal;
                    }
                }else this.property_SBB = true;
        }else if(it.compare(n_son.it) > 0){         //inserir recursividade na subárvore direita
            n_son.right = this.insert(it, n_son, n_son.right, false);
            if(!this.property_SBB)
                if(n_son.right_slope == this.horizontal){
                    if(n_son.right.right_slope == this.horizontal){
                        n_son = this.RightRight(n_son);
                        if(n_father != null)
                            if(son_left) n_father.left_slope = this.horizontal;
                            else n_father.right_slope = this.horizontal;
                    }else if(n_son.right.left_slope == this.horizontal){
                        n_son = this.RightLeft(n_son);
                        if(n_father != null)
                            if(son_left) n_father.left_slope = this.horizontal;
                            else n_father.right_slope = this.horizontal;
                    }
                }else this.property_SBB = true;
        }else{
            //System.out.println("Registro já existe.");
            this.property_SBB = true;
        }
        return n_son;
    }
    /** A busca ocorre percorrendo-se as subárvores a esquerda e a direita da 
     * raíz, por meio das chamadas recursivas até alcançar-se o nó folha da árvore,
     * onde poderá ou não conter o valor requerido; Assim funcionando pelo mesmo 
     * princípio da árvore binária de pesquisa.
     * @param it chave pesquisada
     * @param n árvore da recursão
     * @return 
     */
    private boolean search(Item it, Node n){
        if(n == null) return false;
        //Se a chave it é menor que n.it, busca-se na sub-árvore esquerda, somando +1 ao número de comparações realizadas.
        else if(it.compare(n.it) < 0){ this.number_of_comparisons++; return this.search(it, n.left); }
        //Se a chave it é maior que n.it, busca-se na sub-árvore direita, somando +1 ao número de comparações realizadas.
        else if(it.compare(n.it) > 0){ this.number_of_comparisons++; return this.search(it, n.right); }
        return true;
    }
    
    /** A remoção ocorre percorrendo-se as sub-árvores a esquerda e a direita da 
     * raíz, por meio das chamadas recursivas até alcançar-se o nó a ser retirado;
     * Para excluir um nó deve-se considerar três casos distintos:
     * * remoção da folha: basta remove-lo da árvore, exclui o nó folha e retona a raíz atualizada;
     * * remoção de um nó com um filho: exclui o nó a ser retirado, e o filho substitui a posição do pai;
     * * remoção de um nó com dois filhos: substitui-se o valor do nó a ser retirado
     * pelo valor sucessor, no caso o nó mais à esquerda da subárvore a direita.
     * @param it 
     * @param n árvore da recursão
     * @return 
     */
    private Node remove(Item it, Node n){
      return null;
    }
    
    /** Função recursiva que percorre a árvore binária da esquerda para a direita
     * imprimindo o conteúdo de cada elemento.
     * @param n árvore da recursão
     */    
    private void print(Node n){
        if(n != null){
            this.print(n.left);
            System.out.print(n.it.getKey() + ";");
            this.print(n.right);
        }
    }
}
